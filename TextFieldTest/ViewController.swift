//
//  ViewController.swift
//  TextFieldTest
//
//  Created by Dr. Andreas Plagens on 09.03.19.
//  Copyright © 2019 Dr. Andreas Plagens. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var feld1: UITextField! {
        didSet {
            feld1.delegate = self
        }
    }
    
    @IBOutlet weak var feld2: UITextField!{
        didSet {
            feld2.delegate = self
        }
    }
    
    @IBOutlet weak var feld3: UITextField!{
        didSet {
            feld3.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        print (textField.text)
        return true
    }

}

